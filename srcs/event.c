#include "life_game.h"

void	speed_life(void)
{
	int		i;

	i = 0;
	while (i < (int)WAIT)
		++i;
}

int		loop_event(t_env *env)
{
	int		i;
	int		j;

	i = 0;
	speed_life();
	while (i < env->size_y)
	{
		j = 0;
		while (j < env->size_x)
		{
			env->grid[i][j] == 0 ? new_cellule(i, j, env) : 1;
			env->grid[i][j] == 1 ? survival_cellule(i, j, env) : 0;
			++j;
		}
		++i;
	}
	env->cycle++;
	set_cellule(0, 0, env);
	fill_image(env, 0, 0);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	return (1);
}

int		mouse_event(int but, int x, int y, t_env *env)
{
	if (env->n > 0 && env->grid[y / env->pas][x / env->pas] != 1)
	{
		env->grid[y / env->pas][x / env->pas] = 1;
		env->n--;
	}
//	printf("x %d\n", x);
//	printf("y %d\n", y);
	mouse_fill_image(env, y / env->pas, x / env->pas);
	return (1);
}

int		key_event(int key, t_env *env)
{
	printf("key %d\n", key);
	if (key == KEY_ESC)
		exit(EXIT_SUCCESS);
	else if (key == KEY_SPACE)
	{
		if (mlx_loop_hook(env->mlx, loop_event, env) != 0)
			return (0);
	}
	return (1);
}
