#include "life_game.h"

static int	free_env(t_env *env)
{
	int		i;

	i = 0;
	if (env->grid)
	{
		while (i < env->size_y)
		{
			if (env->grid[i])
			{
				free(env->grid[i]);
				env->grid[i] = NULL;
			}
			++i;
		}
		free(env->grid);
		env->grid = NULL;
	}
	if (env->win)
	{
		mlx_destroy_image(env->mlx, env->img);
		mlx_destroy_window(env->mlx, env->win);
	}
	return (0);
}

static void	init_mlx(t_env *env)
{
	env->mlx = mlx_init();
	env->win = mlx_new_window(env->mlx, WIN_X, WIN_Y, TITLE);
	env->img = mlx_new_image(env->mlx, IMG_X, IMG_Y);
	env->data = (int *)mlx_get_data_addr(env->img, &env->bpp, &env->sl, &env->edn);
	ft_memset(env->data, 0, sizeof(int) * IMG_X * IMG_Y);
}

static int	init_env(t_env *env)
{
	int		i;

	i = 0;
	env->cycle = 0;
	env->start = 0;
	env->pas = PAS;
	env->n = MAX;
	env->size_x = IMG_X / PAS;
	env->size_y = IMG_Y / PAS;
	if (!(env->grid = (int **)ft_memalloc(sizeof(int *) * env->size_y)))
		return (-1);
	while (i < env->size_y)
	{
		if (!(env->grid[i] = (int *)ft_memalloc(sizeof(int) * env->size_x)))
			return (-1);
		ft_memset(env->grid[i], 0, sizeof(int) * env->size_x);
		++i;
	}
	return (1);
}

int			main(void)
{
	t_env	env;

	if (init_env(&env) == -1)
		return (free_env(&env));
	init_mlx(&env);
	life_game(&env);
	return (free_env(&env));
}
