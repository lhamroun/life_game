#include "life_game.h"

void	survival_cellule(int i, int j, t_env *env)
{
	int		voisin;

	voisin = 0;
	if (i - 1 > 0 && j - 1 > 0 && env->grid[i - 1][j - 1] > 0)
		voisin++;
	if (i - 1 > 0 && env->grid[i - 1][j] == 1)
		voisin++;
	if (i - 1 > 0 && j + 1 < env->size_x && env->grid[i - 1][j + 1] > 0)
		voisin++;
	if (j - 1 > 0 && env->grid[i][j - 1] == 1)
		voisin++;
	if (j + 1 < env->size_x && env->grid[i][j + 1] > 0)
		voisin++;
	if (i + 1 < env->size_y && j - 1 > 0 && env->grid[i + 1][j - 1] > 0)
		voisin++;
	if (i + 1 < env->size_y && env->grid[i + 1][j] > 0)
		voisin++;
	if (i + 1 < env->size_y && j + 1 < env->size_x && env->grid[i + 1][j + 1] > 0)
		voisin++;
//	printf("voisin-----> %d\n", voisin);
	if (voisin != 2 && voisin != 3)
	{
//		printf("dlfhekjfhkjezfhekj\n");
		env->grid[i][j] = 2;
	}
}

void	new_cellule(int i, int j, t_env *env)
{
	int		voisin;

	voisin = 0;
	if (i - 1 > 0 && j - 1 > 0 && env->grid[i - 1][j - 1] > 0)
		voisin++;
	if (i - 1 > 0 && env->grid[i - 1][j] > 0)
		voisin++;
	if (i - 1 > 0 && j + 1 < env->size_x && env->grid[i - 1][j + 1] > 0)
		voisin++;
	if (j - 1 > 0 && env->grid[i][j - 1] > 0)
		voisin++;
	if (j + 1 < env->size_x && env->grid[i][j + 1] > 0)
		voisin++;
	if (i + 1 < env->size_y && j - 1 > 0 && env->grid[i + 1][j - 1] > 0)
		voisin++;
	if (i + 1 < env->size_y && env->grid[i + 1][j] > 0)
		voisin++;
	if (i + 1 < env->size_y && j + 1 < env->size_x && env->grid[i + 1][j + 1] > 0)
		voisin++;
	if (voisin == 3)
		env->grid[i][j] = -1;
}

void	set_cellule(int i, int j, t_env *env)
{
	while (i < env->size_y)
	{
		j = 0;
		while (j < env->size_x)
		{
			if (env->grid[i][j] == -1)
				env->grid[i][j] = 1;
			else if (env->grid[i][j] == 2)
				env->grid[i][j] = 0;
			++j;
		}
		++i;
	}
}

void	life_game(t_env *env)
{
	if (mlx_mouse_hook(env->win, mouse_event, env) == 0)
		return ;
	if (mlx_key_hook(env->win, key_event, env) == 0)
		return ;
	mlx_loop(env->mlx);
}
