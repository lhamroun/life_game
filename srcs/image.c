#include "life_game.h"

void	fill_data(t_env *env, int i, int j)
{
	int		x;
	int		y;

	y = 0;
	while (y < env->pas)
	{
		x = 0;
		while (x < env->pas)
		{
			env->data[((y + env->pas * i) * IMG_X) + (x + j * env->pas)] = WHITE;
			++x;
		}
		++y;
	}
}

void	fill_image(t_env *env, int i, int j)
{
//	prints(env);
	ft_memset(env->data, 0, sizeof(int) * IMG_X * IMG_Y);
	while (i < env->size_y)
	{
		j = 0;
		while (j < env->size_x)
		{
			if (env->grid[i][j] == 1)
				fill_data(env, i, j);
			++j;
		}
		++i;
	}
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}

void	mouse_fill_image(t_env *env, int i, int j)
{
//	prints(env);
	if (env->grid[i][j] == 1)
	{
		printf("i --> %d\n", i);
		printf("j --> %d\n\n", j);
		fill_data(env, i, j);
	}
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
