NAME = life_game

FLAGS = #-Wall -Wextra -Werror #-Weverything

HEADER = includes/life_game.h
LIBFT_A = libft/libft.a
INCLUDES = -I includes/ -I libft/ -I minilibx_macos/

LDLIBS = -L libft/ -lft
MLX_LDLIBS = -L./minilibx_macos -lmlx -framework OpenGL -framework AppKit

SRCS_PATH = srcs/
SRCS_NAME = main.c life_game.c event.c image.c print.c
SRCS = $(addprefix $(SRCS_PATH),$(SRCS_NAME))

OBJS_PATH = .objs/
OBJS_NAME = $(SRCS_NAME:.c=.o)
OBJS = $(addprefix $(OBJS_PATH),$(OBJS_NAME))

all: $(NAME)

$(NAME): create_objs_path $(LIBFT_A) $(OBJS)
	$(CC) $(FLAGS) $(INCLUDES) $(MLX_LDLIBS) $(LDLIBS) -o $(NAME) $(OBJS)

create_objs_path:
	mkdir -p $(OBJS_PATH)

$(LIBFT_A):
	make -C libft/
	make -C minilibx_macos/

$(OBJS_PATH)%.o: $(SRCS_PATH)%.c $(HEADER)
	$(CC) $(FLAGS) $(INCLUDES) $(MLX_LDLIBS) $(LDLIBS) -o $@ -c $<

clean:
	$(RM) -rf $(OBJS_PATH)
	make clean -C libft/
	#make clean -C minilibx_macos/

fclean: clean
	$(RM) -rf $(NAME)
	make fclean -C libft/

re: fclean all

.PHONY: re fclean clean all $(NAME)
