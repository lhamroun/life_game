#ifndef LIFE_GAME_H
# define LIFE_GAME_H

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include "libft.h"
# include "mlx.h"

# define WIN_X 1100
# define WIN_Y 800
# define IMG_X 1100
# define IMG_Y 800
# define PAS 8
# define MAX 100
# define WAIT 0000000

# define KEY_ESC 53
# define KEY_SPACE 49

# define TITLE "life_game"

# define WHITE 0xffffff
# define YELLOW 0xffff00

typedef struct	s_env
{
	int		pas;
	int		cycle;
	int		n;//nb de bloc max de depart
	int		start;
	int		size_x;
	int		size_y;
	int		**grid;

	int		sl;
	int		bpp;
	int		edn;
	void	*mlx;
	void	*win;
	void	*img;
	int		*data;
}				t_env;

void	prints(t_env *env);
void	life_game(t_env *env);
int		key_event(int key, t_env *env);
int		loop_event(t_env *env);
int		mouse_event(int but, int x, int y, t_env *env);
void	mouse_fill_image(t_env *env, int i, int j);
void	fill_image(t_env *env, int i, int j);
void	survival_cellule(int i, int j, t_env *env);
void	new_cellule(int i, int j, t_env *env);
void	set_cellule(int i, int j, t_env *env);

# endif
